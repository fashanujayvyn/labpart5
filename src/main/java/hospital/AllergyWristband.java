/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jayvy
 */
import java.util.*;
public class AllergyWristband extends Wristband {
    ArrayList<String> allergies = new ArrayList<String>(); 
    
    public AllergyWristband(Barcode barcode, Info info, ArrayList<String> allergies){
  super(barcode,info);
  this.allergies=allergies;
    }
  
    
}
