/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jayvy
 */
import java.util.*;
public class Wristband {
    
    Barcode barcode;
    private Info info;

    
    public Wristband(Barcode barcode, Info info){
     this.barcode=barcode;
     this.info=info;
     
    }
    
    public Barcode getBarcode(){
        return barcode;
    }
    
    public Info getInfo(){
        
     return info;   
        
    }

    
    public void setBarcode(Barcode barcode){
        this.barcode=barcode;
    }
    
    public void setDetails(Info info){
     this.info=info;   
        
    }

  
}
