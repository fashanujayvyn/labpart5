/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jayvy
 */

import java.util.*;
public class ResearchGroups {
    
    private List<Patient> patient;
    private List<Double> waittime;
    
    
    public ResearchGroups(List <Double> waittime, List<Patient> patient){
     this.patient=patient;
     this.waittime=waittime;
        
    }
    
    public List<Double> getwaittime(){
        
     return waittime;   
    }
    
    public List<Patient> getpatient(){
        
     return patient;   
    }
    
    public void setweighttime(List <Double> waittime){
     this.waittime=waittime;   
    }
     
    public void setPatient(List<Patient> patient){
     
        this.patient=patient;
        
    }
    
}
