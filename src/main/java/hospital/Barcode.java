/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jayvy
 */
public class Barcode {
    
    private String code;
    
    
    public Barcode(String code){
        
     this.code=code;
        
    }
    
    public String getCode(){
        
     return code;   
    }
    
    public void setCode(String code){
        
        
    this.code=code;   
        
    }
        
    
}
